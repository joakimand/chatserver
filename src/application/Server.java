package application;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple server using sockets.
 * Stop button does not work.
 * Closing the GUI window will not stop the application from running in Eclipse.
 * Please stop the application from the console.
 * @author Jocke
 *
 */
public class Server {
	
	private static final int PORT_NUMBER = 6677;
	
	private ServerSocket serverSocket;
	private List<ClientThread> connections = new ArrayList<Server.ClientThread>();
	private int port;
	
	private ServerGUI sGui;
	private boolean isRunning = false;
	
	/**
	 * Starts server in console mode.
	 * @param args
	 */
	/*public static void main(String[] args) {
		Server server = new Server(null, 6677);
		server.start();
	}*/
	
	/**
	 * Constructor 
	 */
	public Server(int port) {
		this(null, port);
	}
	
	/**
	 * Constructor
	 */
	public Server(ServerGUI sGui, int port) {
		this.sGui = sGui;
		this.port = port;
	}
	
	/**
	 * GUI reference for server.
	 * @param sGui
	 */
	public void setGUI(ServerGUI sGui) {
		this.sGui = sGui;
	}

	/**
	 * Start the server.
	 * Listens for client connections.
	 * Will run until said not to. 
	 */
	public void start() {
		try {
			displayEvent("Starting server @ port: " + PORT_NUMBER + "...");
//			System.out.println("Starting server @ port: " + PORT_NUMBER + "...");
			
			serverSocket = new ServerSocket(PORT_NUMBER, 100);
			isRunning = true;
			displayEvent("Server started.");
//			System.out.println("Server started.");
			new ClientListener().start();
			
		} catch (IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * Stops server.
	 * Closing server socket.
	 */
	public void stop() {
		isRunning = false;
		try {
			for (ClientThread clientThread : connections) {
				clientThread.socket.close();
			}
//			connections.removeAll(connections);
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends an event message from the server to the GUI.
	 */
	private void displayEvent(String message) {
		if (sGui != null) sGui.writeToGUI(message);
	}
	
	/**
	 * Broadcasts message sent by a client to all clients in list.
	 * @param message
	 */
	public synchronized void broadcast(String message)  {
//		System.out.println(message);
		if (connections != null)
		for (ClientThread clientThread : connections) {
			if (clientThread != null) clientThread.writeMessage(message);
		}
	}
	
	/**
	 * Listens for connections.
	 * @author Jocke
	 *
	 */
	class ClientListener extends Thread {
		@Override
		public void run() {
//			displayEvent("Starting client listener...");
			while (isRunning) {
				try {
//					displayEvent("Listening for clients...");
					Socket socket = serverSocket.accept();
//					System.out.println("Client connected at: " + socket.getInetAddress().getHostName());
					displayEvent("Client found: " + socket.getInetAddress().getHostName() + " at: " + socket.getInetAddress().getHostAddress());
//					System.out.println(socket.getInetAddress().getHostName() + " " + socket.getInetAddress().getHostAddress());
					ClientThread newClient = new ClientThread(socket);
					connections.add(newClient);
					newClient.start();
					
//					displayEvent("Client connected!"/* + newClient.getUsername()*/);
					
				} catch (IOException e) {
//					e.printStackTrace();
					displayEvent(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Each client gets their own thread on the server.
	 * @author Jocke
	 *
	 */
	class ClientThread extends Thread {
		
		private Socket socket;
		private ObjectInputStream input;
		private ObjectOutputStream output;
		private String username;
		
		/**
		 * Constructor.
		 * Opens streams for client.
		 * @param socket
		 */
		public ClientThread(Socket socket) {
			this.socket = socket;
			try {
//				System.out.println(socket);
				
				output = new ObjectOutputStream(socket.getOutputStream());
//				System.out.println("Output");
				output.flush();
//				System.out.println("Flushed");
				
				input = new ObjectInputStream(socket.getInputStream());
//				System.out.println("Input");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} 
		
		@Override
		public void run() {
			try {
				while (true) {
					String message = (String) input.readObject();
					sGui.writeToGUI(message);
					broadcast(message);
				}
				
			} catch (Exception e) {
//				TODO: Change to username.
				displayEvent(socket.getLocalAddress() + " disconnected from the server.");
				connections.remove(this);
			}
		}
		
		/**
		 * Writes a message to output stream for client.
		 * 
		 * @param message The message.
		 */
		public void writeMessage(String message) {
			try {
				output.writeObject(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public String getUsername() {
			return username;
		}
	}
}
