package application.view;

import application.ServerGUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * GUI controller class for a server.
 * @author Jocke
 *
 */
public class ServerGUIController {

	private ServerGUI sGui;
	private static final String SERVER_NAME = "[Server]: ";
	
	@FXML
	private TextArea displayWindow;
	@FXML
	private TextField inputField;
	@FXML
	private Button startButton;
	@FXML
	private Button stopButton;
	
	/**
	 * Initialize is run before the constructor.
	 * Does nothing as of yet.
	 */
	public void initialize() {
	}
	
	/**
	 * Give the controller a reference to the main GUI.
	 * @param sGui
	 */
	public void setGUI(ServerGUI sGui) {
		this.sGui = sGui;
	}
	
	/**
	 * Clears the input field.
	 */
	public void clearInputField() {
		inputField.clear();
	}
	
	/**
	 * Displays the message on screen.
	 */
	public void displayMessage(final String message) {
		displayWindow.appendText(message + "\n");
	}
	
	/**
	 * Upon hitting the enter key (but not shift + enter in combination).
	 * Displays inputed message on screen before sending it to the main GUI class.
	 * The input field is then cleared.
	 * 
	 * The main GUI class should send the message to the server who then broadcasts it to all connected clients in list.
	 * @param event
	 */
	@FXML
	public void onEnterPressed(KeyEvent event) {
//		System.out.println(event.getCode());
		if (event.getCode().equals(KeyCode.ENTER) && !event.isShiftDown()) {
//			Message to display is the server's name + what we wrote.
			String message = SERVER_NAME + inputField.getText();
			displayMessage(message);
			sGui.sendMessage(message);
			clearInputField();
		}
	}
	
	/**
	 * If the start button is clicked, start the server.
	 * Then disable the start button.
	 * Starting the server enables the stop button.
	 * @param event
	 */
	@FXML
	public void onStartClicked(ActionEvent event) {
		sGui.startServer();
		startButton.setDisable(true);
		stopButton.setDisable(false);
		inputField.setEditable(true);
	}
	
	/**
	 * If the stop button is pressed, stop the server.
	 * Then disable the stop button.
	 * Stopping the server enables the start button.
	 * @param event
	 */
	@FXML
	public void onStopClicked(ActionEvent event) {
		sGui.stopServer();
		startButton.setDisable(false);
		stopButton.setDisable(true);
		inputField.setEditable(false);
	}
	
}
