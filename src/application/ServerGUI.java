package application;

import application.view.ServerGUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ServerGUI extends Application {

	private static final String FXML_PATH = "view/ServerGUI.fxml";
	private ServerGUIController sController;
	private Server server;

	private GridPane root;

	/**
	 * Creates and shows the window. Create a new server reference.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass()
					.getResource(FXML_PATH));
			root = loader.load();

			sController = loader.getController();
			sController.setGUI(this);

			Scene scene = new Scene(root);

			primaryStage.setScene(scene);

			primaryStage.setOnCloseRequest(e -> {

				try {
					server.stop();
				} finally {
					System.exit(0);
				}

			});

			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write text to the GUI.
	 * 
	 * @param message
	 */
	public void writeToGUI(String message) {
		sController.displayMessage(message);
	}

	/**
	 * Send message to server. Broadcast the message.
	 * 
	 * @param message
	 */
	public void sendMessage(final String message) {
		server.broadcast(message);
	}

	/**
	 * Start the server.
	 */
	public void startServer() {
		server = new Server(this, 6677);
		server.start();
	}

	/**
	 * Stop the server.
	 */
	public void stopServer() {
		if (server != null)
			server.stop();
		server = null;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
